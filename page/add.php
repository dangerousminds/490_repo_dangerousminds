<?php
// This script performs an INSERT query to add a record to the clients table.
session_start();
//check the session
if (!isset($_SESSION['username'])){
echo "You are not logged in!";
exit();
}else{

$page_title = 'Add Client';
include ('includes/header.html');

// Check for form submission:
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	require ('mysqli_connect.php'); // Connect to the db.
		
	$errors = array(); // Initialize an error array.
	
	// Check for a name:
	if (empty($_POST['name'])) {
		$errors[] = 'You forgot to enter s name.';
	} else {
		$n = mysqli_real_escape_string($dbc, trim($_POST['name']));
	}
	// Check for a phone number:
	if (empty($_POST['phone'])) {
		$errors[] = 'You forgot to enter a phone number.';
	} else {
		$p = mysqli_real_escape_string($dbc, trim($_POST['phone']));
	}
	// Check for an email address:
	if (empty($_POST['email'])) {
		$errors[] = 'You forgot to enter an email address.';
	} else {
		$e = mysqli_real_escape_string($dbc, trim($_POST['email']));
	}
		// Check for a package type:
	if (empty($_POST['package_type'])) {
		$errors[] = 'You forgot to enter an email address.';
	} else {
		$pa = mysqli_real_escape_string($dbc, trim($_POST['package_type']));
	}
		// Check for an event date:
	if (empty($_POST['event_date'])) {
		$errors[] = 'You forgot to enter an event date.';
	} else {
		$ed = mysqli_real_escape_string($dbc, trim($_POST['event_date']));
	}
		// Check for the contract status:
	if (empty($_POST['contract_signed'])) {
		$errors[] = 'You forgot to enter an email address.';
	} else {
		$cs = mysqli_real_escape_string($dbc, trim($_POST['contract_signed']));
	}
	
	if (empty($errors)) { // If everything's OK.
	
		// Register the user in the database...
		
		// Make the query:
		$q = "INSERT INTO clients (name, phone, email, package_type, event_date, contract_signed) VALUES ('$n', '$p', '$e', '$pa', '$ed', '$cs')";		
		$r = @mysqli_query ($dbc, $q); // Run the query.
		if ($r) { // If it ran OK.
		
			// Print a message:
			echo '<h1>Thank you!</h1>
		<p>The client has been entered.</p><p><br /></p>';	
		
		} else { // If it did not run OK.
			
			// Public message:
			echo '<h1>System Error</h1>
			<p class="error">You could not be registered due to a system error. We apologize for any inconvenience.</p>'; 
			
			// Debugging message:
			echo '<p>' . mysqli_error($dbc) . '<br /><br />Query: ' . $q . '</p>';
						
		} // End of if ($r) IF.
		
		mysqli_close($dbc); // Close the database connection.

		// Include the footer and quit the script:
		include ('includes/footer.html'); 
		exit();
		
	} else { // Report the errors.
	
		echo '<h1>Error!</h1>
		<p class="error">The following error(s) occurred:<br />';
		foreach ($errors as $msg) { // Print each error.
			echo " - $msg<br />\n";
		}
		echo '</p><p>Please try again.</p><p><br /></p>';
		
	} // End of if (empty($errors)) IF.
	
	mysqli_close($dbc); // Close the database connection.

} // End of the main Submit conditional.
?>
<h1>Add a Client</h1>
<form action="add.php" method="post">
	<p>Name:<br> <input type="text" name="name" size="20" maxlength="100" value="<?php if (isset($_POST['name'])) echo $_POST['name']; ?>" /></p>
	<p>Phone:<br> <input type="text" name="phone" size="20" maxlength="40" value="<?php if (isset($_POST['phone'])) echo $_POST['phone']; ?>" /></p>
	<p>Email Address:<br> <input type="text" name="email" size="20" maxlength="60" value="<?php if (isset($_POST['email'])) echo $_POST['email']; ?>"  /> </p>
	<p>Package Type:<br> <input type="text" name="package_type" size="20" maxlength="60" value="<?php if (isset($_POST['package_type'])) echo $_POST['package_type']; ?>"  /> </p>
	<p>Event Date (YYYY-MM-DD):<br> <input type="text" name="event_date" size="20" maxlength="60" value="<?php if (isset($_POST['event_date'])) echo $_POST['event_date']; ?>"  /> </p>
	<p>Is the contract signed? (Yes or No):<br> <input type="text" name="contract_signed" size="20" maxlength="3" value="<?php if (isset($_POST['contract_signed'])) echo $_POST['contract_signed']; ?>"  /> </p>
	<p><input type="submit" id="submit" name="submit" value="Submit" /></p>
</form>
<?php include ('includes/footer.html'); 
}
?>
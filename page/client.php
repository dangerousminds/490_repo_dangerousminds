<?php
// This script retrieves all the records from the clients table.
// This new version allows the results to be sorted in different ways.
session_start();
//check the session
if (!isset($_SESSION['username'])){
echo "You are not logged in!";
exit();
}else{

$page_title = 'View the Current Clients';
include ('includes/header.html');
echo '<h1>Clients</h1>';

require ('mysqli_connect.php');

// Number of records to show per page:
$display = 10;

// Determine how many pages there are...
if (isset($_GET['p']) && is_numeric($_GET['p'])) { // Already been determined.
	$pages = $_GET['p'];
} else { // Need to determine.
 	// Count the number of records:
	$q = "SELECT COUNT(ID) FROM clients";
	$r = @mysqli_query ($dbc, $q);
	$row = @mysqli_fetch_array ($r, MYSQLI_NUM);
	$records = $row[0];
	// Calculate the number of pages...
	if ($records > $display) { // More than 1 page.
		$pages = ceil ($records/$display);
	} else {
		$pages = 1;
	}
} // End of p IF.

// Determine where in the database to start returning results...
if (isset($_GET['s']) && is_numeric($_GET['s'])) {
	$start = $_GET['s'];
} else {
	$start = 0;
}

// Determine the sort...
// Default is by registration date.
$sort = (isset($_GET['sort'])) ? $_GET['sort'] : 'ID';

// Determine the sorting order:
switch ($sort) {
	case 'ID':
		$order_by = 'ID ASC';
		break;
	case 'n':
		$order_by = 'name ASC';
		break;
	case 'p':
		$order_by = 'phone ASC';
		break;
	case 'e':
		$order_by = 'email ASC';
		break;
	case 'pa':
		$order_by = 'package_type ASC';
		break;
	case 'ed':
		$order_by = 'event_date ASC';
		break;
	case 'cs':
		$order_by = 'contract_signed ASC';
		break;
	default:
		$order_by = 'ID ASC';
		$sort = 'ID';
		break;
}
	
// Define the query:
$q = "SELECT ID, name, phone, email, package_type, DATE_FORMAT(event_date, '%M %d, %Y') AS ed, contract_signed FROM clients ORDER BY $order_by LIMIT $start, $display";		
$r = @mysqli_query ($dbc, $q); // Run the query.

// Table header:
echo '<table align="center" cellspacing="0" cellpadding="5" width="75%">
<tr>
	<td align="left"><b>Edit</b></td>
	<td align="left"><b>Delete</b></td>
	<td align="left"><b><a href="client.php?sort=ID">ID</a></b></td>
	<td align="left"><b><a href="client.php?sort=n">Name</a></b></td>
	<td align="left"><b><a href="client.php?sort=p">Phone</a></b></td>
	<td align="left"><b><a href="client.php?sort=e">Email</a></b></td>
	<td align="left"><b><a href="client.php?sort=pa">Package</a></b></td>
	<td align="left"><b><a href="client.php?sort=ed">Event Date</a></b></td>
	<td align="left"><b><a href="client.php?sort=cs">Contract Signed</a></b></td>
</tr>
';

// Fetch and print all the records....
$bg = '#eeeeee'; 
while ($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
	$bg = ($bg=='#eeeeee' ? '#ffffff' : '#eeeeee');
		echo '<tr bgcolor="' . $bg . '">
		<td align="left"><a href="edit.php?id=' . $row['ID'] . '">Edit</a></td>
		<td align="left"><a href="delete.php?id=' . $row['ID'] . '">Delete</a></td>
		<td align="left">' . $row['ID'] . '</td>
		<td align="left">' . $row['name'] . '</td>
		<td align="left">' . $row['phone'] . '</td>
		<td align="left">' . $row['email'] . '</td>
		<td align="left">' . $row['package_type'] . '</td>
		<td align="left">' . $row['ed'] . '</td>
		<td align="left">' . $row['contract_signed'] . '</td>
	</tr>
	';
} // End of WHILE loop.

echo '</table>';
mysqli_free_result ($r);
mysqli_close($dbc);

// Make the links to other pages, if necessary.
if ($pages > 1) {
	
	echo '<br /><p>';
	$current_page = ($start/$display) + 1;
	
	// If it's not the first page, make a Previous button:
	if ($current_page != 1) {
		echo '<a href="client.php?s=' . ($start - $display) . '&p=' . $pages . '&sort=' . $sort . '">Previous</a> ';
	}
	
	// Make all the numbered pages:
	for ($i = 1; $i <= $pages; $i++) {
		if ($i != $current_page) {
			echo '<a href="client.php?s=' . (($display * ($i - 1))) . '&p=' . $pages . '&sort=' . $sort . '">' . $i . '</a> ';
		} else {
			echo $i . ' ';
		}
	} // End of FOR loop.
	
	// If it's not the last page, make a Next button:
	if ($current_page != $pages) {
		echo '<a href="client.php?s=' . ($start + $display) . '&p=' . $pages . '&sort=' . $sort . '">Next</a>';
	}
	
	echo '</p>'; // Close the paragraph.
	
} // End of links section.
	
include ('includes/footer.html');
}
?>
<?php 
// This page is for editing a client record.
// This page is accessed through client.php.
session_start();
//check the session
if (!isset($_SESSION['username'])){
echo "You are not logged in!";
exit();
}else{

$page_title = 'Edit Client';
include ('includes/header.html');
echo '<h1>Edit Client</h1>';

// Check for a valid user ID, through GET or POST:
if ( (isset($_GET['id'])) && (is_numeric($_GET['id'])) ) { // From view_clients.php
	$id = $_GET['id'];
} elseif ( (isset($_POST['id'])) && (is_numeric($_POST['id'])) ) { // Form submission.
	$id = $_POST['id'];
} else { // No valid ID, kill the script.
	echo '<p class="error">This page has been accessed in error.</p>';
	include ('includes/footer.html'); 
	exit();
}

require ('mysqli_connect.php'); 

// Check if the form has been submitted:
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	$errors = array();
	
	// Check for a name:
	if (empty($_POST['name'])) {
		$errors[] = 'You forgot to enter the name.';
	} else {
		$fn = mysqli_real_escape_string($dbc, trim($_POST['name']));
	}
	if (empty($_POST['phone'])) {
		$errors[] = 'You forgot to enter a phone number.';
	} else {
		$fn = mysqli_real_escape_string($dbc, trim($_POST['phone']));
	}
	if (empty($_POST['email'])) {
		$errors[] = 'You forgot to enter the email.';
	} else {
		$fn = mysqli_real_escape_string($dbc, trim($_POST['email']));
	}
	if (empty($_POST['package_type'])) {
		$errors[] = 'You forgot to enter the package.';
	} else {
		$fn = mysqli_real_escape_string($dbc, trim($_POST['package_type']));
	}
	if (empty($_POST['event_date'])) {
		$errors[] = 'You forgot to enter the event date.';
	} else {
		$fn = mysqli_real_escape_string($dbc, trim($_POST['event_date']));
	}
	if (empty($_POST['contract_signed'])) {
		$errors[] = 'You forgot to enter the contract status.';
	} else {
		$fn = mysqli_real_escape_string($dbc, trim($_POST['contract_signed']));
	}
	
	
	$id = $_POST['id'];

	$name = $_POST['name'];
	$phone = $_POST['phone'];
	$email = $_POST['email'];
	$package_type = $_POST['package_type'];
	$event_date = $_POST['event_date'];
	$contract_signed = $_POST['contract_signed'];

	if (empty($errors)) { // If everything's OK.
		$q = "UPDATE clients SET name='$name', phone='$phone', email='$email', package_type='$package_type', event_date='$event_date', contract_signed='$contract_signed' WHERE id=$id LIMIT 1";
		$r = @mysqli_query ($dbc, $q);
		if (mysqli_affected_rows($dbc) == 1) { // If it ran OK.

			// Print a message:
			echo '<p>The client has been edited.</p>';	
				
		} else { // If it did not run OK.
			echo '<p class="error">The client could not be edited due to a system error.</p>'; // Public message.
			echo '<p>' . mysqli_error($dbc) . '<br />Query: ' . $q . '</p>'; // Debugging message.
		}
				
	} else { // Report the errors.

		echo '<p class="error">The following error(s) occurred:<br />';
		foreach ($errors as $msg) { // Print each error.
			echo " - $msg<br />\n";
		}
		echo '</p><p>Please try again.</p>';
	
	} // End of if (empty($errors)) IF.

} // End of submit conditional.

// Always show the form...

// Retrieve the user's information:
$q = "SELECT ID, name, phone, email, package_type, event_date, contract_signed FROM clients WHERE id=$id";		
$r = @mysqli_query ($dbc, $q);

if (mysqli_num_rows($r) == 1) { // Valid ID, show the form.

	// Get the user's information:
	$row = mysqli_fetch_array ($r, MYSQLI_NUM);
	
	// Create the form:
	echo '<form action="edit.php" method="post">
    <p>Name:<br> <input type="text" name="name" size="20" maxlength="50" value="' . $row[1] . '" /></p>
    <p>Phone:<br> <input type="text" name="phone" size="20" maxlength="30" value="' . $row[2] . '" /></p>
    <p>Email:<br> <input type="text" name="email" size="20" maxlength="60" value="' . $row[3] . '"  /> </p>
    <p>Package Type:<br> <input type="text" name="package_type" size="20" maxlength="60" value="' . $row[4] . '"  /> </p>
    <p>Event Date (YYYY-MM-DD):<br> <input type="text" name="event_date" size="20" maxlength="60" value="' . $row[5] . '"  /> </p>
    <p>Is there a signed contract? (Yes or No):<br> <input type="text" name="contract_signed" size="20" maxlength="3" value="' . $row[6] . '"  /> </p>
    <p><input type="submit" id="submit" name="submit" value="Submit" /></p>
    <input type="hidden" name="id" value="' . $id . '" />
    </form>';

} else { // Not a valid ID.
	echo '<p class="error">This page has been accessed in error.</p>';
}

mysqli_close($dbc);
		
include ('includes/footer.html');
}
?>
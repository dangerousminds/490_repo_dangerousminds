<?php
session_start();
require('mysqli_connect.php');

If(isset($_POST['username']) and isset($_POST['password'])){
	$username = $_POST['username'];
	$password = $_POST['password'];
	$query = "SELECT * FROM `users` WHERE username='$username' and password='$password'";
	
	$result = mysqli_query($dbc, $query) or die(mysqli_error($dbc));
	$count = mysqli_num_rows($result);
	
	if ($count == 1){
	$_SESSION['username'] = $username;
	header('Location: ../page/client.php');
	}else{
	//Error message
	echo "Invalid Login Credentials.";
	}
}

?>

<!DOCTYPE html>
<html>
<head>
	<title>Admin Login</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	  <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/main.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="https://use.fontawesome.com/ee78635074.js"></script>
</head>
<body>

	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h1 class="text-center">Admin</h1>			
			</div>	
			<div class="modal-body">
				<form class="col-mod-12 center-block" id="login" action="login.php" method="post">
					<div class="form-group">
						<input type="text" class="form-control input-lg" placeholder="Username" name="username" id="username">
					</div>
					<div class="form-group">
						<input type="password" class="form-control input-lg" placeholder="Password" name="password" id="password">
					</div>
					<div class="form-group">
						<input type="submit" class="btn btn-block btn-lg btn-primary" name="submit" value="Login">						
					</div>
				</form>
				<div class="modal-footer">
					<div class="col-md-12">
						<a href="../index.html"><button class="btn">Cancel</button></a>
					</div>					
				</div>
			</div>
		</div>	
	</div>


</body>
</html>